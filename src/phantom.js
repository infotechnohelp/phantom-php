var page = require('webpage').create();
var fs = require('fs');
var args = require('system').args;

var url = args[1], outputHtml = args[2], outputCapture = args[3], timeout = args[4];


function dump() {
    if (outputHtml !== false && outputHtml !== undefined) {
        fs.write(outputHtml, page.content, 'w');
    }

    if (outputCapture !== false && outputCapture !== undefined) {
        page.render(outputCapture);
    }
    phantom.exit();
}


try {
    outputHtml = JSON.parse(outputHtml);
} catch (error) {
}

try {
    outputCapture = JSON.parse(outputCapture);
} catch (error) {
}

page.open(url);

page.onLoadFinished = function () {
    if (timeout !== undefined) {
        window.setTimeout(function () {
            dump();
        }, parseInt(timeout));
    } else {
        dump();
    }
};


