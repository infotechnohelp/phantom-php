<?php

namespace Infotechnohelp\PhantomPhp;

/**
 * Class PhantomPhp
 * @package Infotechnohelp\PhantomPhp
 */
class PhantomPhp
{
    public function outputHtml(string $url, string $output, int $timeout = null)
    {
        exec(sprintf('phantomjs %sphantom.js "%s" "%s" false %s',
            dirname(__FILE__) . DIRECTORY_SEPARATOR, $url, $output, $timeout));
    }

    public function getHtml(string $url, int $timeout = null)
    {
        $tmpFilePath = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'tmp/' . time() . '.html';

        exec(sprintf('phantomjs %sphantom.js "%s" "%s" false %s',
            dirname(__FILE__) . DIRECTORY_SEPARATOR, $url, $tmpFilePath, $timeout));

        $result = file_get_contents($tmpFilePath);

        unlink($tmpFilePath);

        return $result;
    }

    public function outputPageCapture(string $url, string $output, int $timeout = null)
    {
        exec(sprintf('phantomjs %sphantom.js "%s" false "%s" %s',
            dirname(__FILE__) . DIRECTORY_SEPARATOR, $url, $output, $timeout));
    }

    public function dumpPage(string $url, $outputHtml, $outputCapture = false, int $timeout = null)
    {
        $outputHtml = (!$outputHtml) ? 'false' : $outputHtml;
        $outputCapture = (!$outputCapture) ? 'false' : $outputCapture;

        exec(sprintf('phantomjs %sphantom.js "%s" "%s" "%s" %s',
            dirname(__FILE__) . DIRECTORY_SEPARATOR, $url, $outputHtml, $outputCapture, $timeout));
    }
}