<?php

namespace Infotechnohelp\PhantomPhp;

use Infotechnohelp\SimpleHtmlDom\SimpleHtmlDom;

/**
 * Class Client
 * @package Infotechnohelp\PhantomPhp
 */
class Client
{
    /**
     * @var int
     */
    private $timeout = 0;
    /**
     * @var int
     */
    private $timeoutStep = 1000;
    /**
     * @var int
     */
    private $timeoutLimit = 10000;

    /**
     * @var SimpleHtmlDom
     */
    protected $html;

    /**
     * NameComClient constructor.
     */
    public function __construct()
    {
        $this->html = new SimpleHtmlDom();
    }

    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param string $url
     * @param $validationCallable
     * @return string|null
     */
    protected function getValidResponse(string $url, $validationCallable): ?string
    {
        for ($i = $this->timeout; $i <= $this->timeoutLimit; $i = $i + $this->timeoutStep) {

            $response = (new PhantomPhp())->getHtml($url, $i);

            if ($validationCallable($response)) {
                $this->timeout = $i;
                return $response;
            }
        }

        return false;
    }
}